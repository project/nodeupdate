
Drupal.nodeupdate = {};

Drupal.nodeupdate.checkUpdate = function() {
  $.ajax({
    type: 'POST',
    url: Drupal.settings.basePath+'nodeupdate/'+Drupal.settings.nodeupdate.nid+'/'+Drupal.settings.nodeupdate.time, 
    dataType: 'json',
    success: function(data) {
      if (data.changed) {
        clearInterval(Drupal.nodeupdate.timer);
        if (data.changed == -1) {
          alert(Drupal.settings.nodeupdate.messageDeleted);
          location.href = Drupal.settings.basePath;
        }
        else if (confirm(Drupal.settings.nodeupdate.messageUpdated)) {
          location.reload(true);
        }
      }
    }
  });
}

$(document).ready(function() {
  Drupal.nodeupdate.timer = setInterval('Drupal.nodeupdate.checkUpdate()', Drupal.settings.nodeupdate.interval);
});

