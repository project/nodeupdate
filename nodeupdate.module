<?php

function nodeupdate_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  $types = variable_get('nodeupdate_types', NULL);
  if (isset($types) && !$types[$node->type]) return;
  switch ($op) {
  case 'view':
    if ($a3 == TRUE) break; // teaser?
    _nodeupdate_inject($node->nid);
    break;
  case 'insert':
  case 'update':
    cache_set('nodeupdate:'. $node->nid, array('changed' => $node->changed), 'cache');
    break;
  case 'delete':
    cache_clear_all('nodeupdate:'. $node->nid, 'cache');
    break;
  }
}

function nodeupdate_form_alter(&$form, $form_state, $form_id) {
  if (!isset($form['type']) || !isset($form['nid']['#value'])) return;
  $types = variable_get('nodeupdate_types', NULL);
  if (isset($types) && !$types[$form['type']['#value']]) return;
  _nodeupdate_inject(intval($form['nid']['#value']));
}

function nodeupdate_menu() {
  $items['nodeupdate'] = array(
    'title' => 'Node update',
    'description' => 'Check if the node has been updated since a given time.',
    'page callback' => '_nodeupdate_check',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );
  $items['admin/settings/nodeupdate'] = array(
    'title' => 'Node update settings',
    'description' => 'Administer settings for the node update notification.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('_nodeupdate_settings'),
    'access arguments' => array('administer site settings'),
  );
  return $items;
}

function _nodeupdate_inject($nid) {
  drupal_add_js(drupal_get_path('module', 'nodeupdate').'/nodeupdate.js');
  drupal_add_js(array('nodeupdate' => array(
    'nid' => $nid,
    'time' => time(),
    'messageUpdated' => t('This node has been updated. Would you like to reload it?'),
    'messageDeleted' => t('This node has been deleted. We will navigate away from it.'),
    'interval' => variable_get('nodeupdate_interval', 5000),
  )), 
  'setting'
  );
}

function _nodeupdate_last_changed($nid) {
  if (($cache = cache_get('nodeupdate:'. $nid, 'cache')) && !empty($cache->data)) {
    $changed = $cache->data['changed'];
  }
  else {
    $changed = node_last_changed($nid);
    if ($changed) { // NULL if deleted
      cache_set('nodeupdate:'. $nid, array('changed' => $changed), 'cache');
    }
  }
  return $changed;
}

function _nodeupdate_check($nid, $time) {
  $changed = _nodeupdate_last_changed($nid);
  drupal_json(array('changed' => (!$changed ? -1 : ($changed > $time ? $changed : 0))));
}

function _nodeupdate_settings() {
  $form['nodeupdate_interval'] = array(
    '#type' => 'textfield',
    '#title' => t('Refresh interval'),
    '#default_value' => variable_get('nodeupdate_interval', 5000),
    '#field_suffix' => t('milliseconds'),
    '#description' => t('The time interval before the module checks again if the node has been updated.'),
  );
  $form['nodeupdate_types'] = array(
    '#title' => t('Content types'),
    '#type' => 'checkboxes',
    '#options' => node_get_types('names'),
    '#default_value' => variable_get('nodeupdate_types', NULL),
    '#description' => t('Content types that the module will intercept to check for node updates.'),
  );
  return system_settings_form($form);
}

